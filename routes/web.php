<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

Auth::routes();

Route::get('/home', 'AdminController@index');
Route::get('/photo', 'AdminController@photo');
Route::post('/news/qoshish/save','AdminController@News_save');
Route::get('/news/delete/{id}','AdminController@News_delete');
Route::get('/news/edit/{id}','AdminController@News_edit');
Route::post('/news/edit/save/{id}','AdminController@News_edit_save');
Route::get('/news/qoshish',function(){
    $page=(int)\request()->get('page');
    return view('admin.add',['active'=>1,'page'=>$page]);
});


Route::get('/rasm/qoshish','AdminController@rasm_qosh');
Route::post('/rasm/qoshish/save','AdminController@rasm_save');
Route::get('/rasm/delete/{id}','AdminController@rasm_delete');

Route::get('/teacher','AdminController@teacher');
Route::get('/teacher/qoshish',function(){
    $page=(int)\request()->get('page');
    return view('admin.teacher_add',['active'=>3,'page'=>$page]);
});
Route::post('/teacher/qoshish/save','AdminController@teacher_save');
Route::get('/teacher/delete/{id}','AdminController@teacher_delete');
Route::get('/teacher/edit/{id}','AdminController@teacher_edit');
Route::post('/teacher/edit/save/{id}','AdminController@teacher_edit_save');


Route::get('/student','AdminController@student');
Route::get('/student/qoshish',function(){
    return view('admin.student_add',['active'=>4]);
});
Route::post('/student/qoshish/save','AdminController@student_save');
Route::get('/student/delete/{id}','AdminController@student_delete');
Route::get('/student/edit/{id}','AdminController@student_edit');
Route::post('/student/edit/save/{id}','AdminController@student_edit_save');


Route::get('/raxbariyat','AdminController@raxbariyat');
Route::get('/raxbariyat/qoshish',function(){
    return view('admin.raxbariyat_add',['active'=>6]);
});
Route::post('/raxbariyat/qoshish/save','AdminController@raxbariyat_save');
Route::get('/raxbariyat/delete/{id}','AdminController@raxbariyat_delete');
Route::get('/raxbariyat/edit/{id}','AdminController@raxbariyat_edit');
Route::post('/raxbariyat/edit/save/{id}','AdminController@raxbariyat_edit_save');


Route::get('/library','AdminController@library');
Route::get('/library/qoshish','AdminController@library_qoshish');
Route::post('/library/qoshish/save','AdminController@library_save');
Route::get('/library/edit/{id}','AdminController@library_edit');
Route::post('/library/edit/save/{id}','AdminController@library_edit_save');


Route::get('/maktab','HomeController@maktab');
Route::get('/gallerya','HomeController@gallerya');
Route::get('/yangilik/{id}','HomeController@yangilik');
Route::get('/manager','HomeController@manager');
Route::get('/manager/{id}','HomeController@manager_id');
Route::get('/teachers','HomeController@teacher');
Route::get('/talabalar','HomeController@talabalar');
Route::get('/kutubxona','HomeController@kutubxona');
Route::get('/download/{id}','HomeController@download');