@extends('layouts.user')

@section('content')

<section id="" class="portfolio">
    <div class="" data-aos="fade-up">
        <div class="row">
            <div class="col-12 col-md-8 ">
                <h5 class="card-title">{{$new->titil}}</h5>
                    <div class="card w-100" >
                        <img src="{{asset('/storage/news/'.$new->img)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      
                      <p class="card-text">{{$new->mss}}</p>
                      <i class="icofont-ui-calendar"></i>{{$new->created_at}}
                    </div>
                  </div>
            </div>
            <div class="col-12 col-md-4 "  style="
            background-color: #f2f2f2;">
                
    <div class="section-title">
        <h2 id="as1">Yangiliklar</h2>
      </div>
                @foreach($news as $new)
                    <ul id="new{{$d++}}" class="list-group list-group-flush" style="display: none">
                        <li class="list-group-item" style="border-bottom: 5px solid rgb(12, 132, 211);"><a class="text-body" href="/yangilik/{{$new->id}}"><b><i class="icofont-ui-calendar"></i>{{$new->created_at}}<br>  {{$new->titil}}</b></a></li>
                
                    </ul>
                @endforeach 
                <button id="yana" class="btn btn-primary mt-5">Ko'proq ko'rish</button>
            </div>
        </div>
    </div>
  </section>
@endsection