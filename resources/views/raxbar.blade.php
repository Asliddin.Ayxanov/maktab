@extends('layouts.user')

@section('content')

<section id="" class="team section-bg">
    <div class="container" data-aos="fade-up">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 ">
                <div class="section-title">
                    <h2>Maktab Raxbariyati</h2>
                </div>
                <div class="row">
                    @foreach ($raxbariyat as $raxbar)
                    <a href="/manager/{{$raxbar->id}}">
                    <div class="col-12">
                        <div class="member d-flex align-items-center" data-aos="zoom-in" data-aos-delay="100">
                            <div style="overflow: hidden;width: 180px;">
                                <img src="{{ asset('storage/raxbariyat')}}/{{$raxbar->img}}" class="img-fluid" alt="">
                            </div>
                            <div class="member-info text-center ">
                                <h4>{{$raxbar->name}}</h4>
                                <span>Jizzax viloyati G'allaorol tumani 54-maktab {{$raxbar->lavozim}}</span>
                               <hr style="border: 3px splid blue">
                               <div class="email mb-5">
                               
                                <h4> <i class="icofont-envelope"></i>Email:   {{$raxbar->email}}</h4>
                                
                              </div>
                
                              <div class="phone mt-5">
                               
                                <h4> <i class="icofont-phone"></i>Tell:   {{$raxbar->tel}}</h4>
                              </div>
                               
                                
                            </div>
                        </div>
                    </div></a>
                    @endforeach
                    
                </div>
               
                
             
            </div>
           
        </div>
    </div>
  </section>
@endsection