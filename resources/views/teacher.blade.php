@extends('layouts.user')

@section('content')

<section id="" class="team section-bg">
    <div class="container" data-aos="fade-up">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 ">
                <div class="section-title">
                    <h2>Maktab O'qituvchilari</h2>
                </div>
                                    
                    <div class="d-flex mb-4 mt-1">
                        <div class="text-center" style="width: 20%">Rasm</div>
                        <div class="text-center" style="width: 20%">FIO</div>
                        <div class="text-center" style="width: 20%">Tug'ulgan sana</div>
                        <div class="text-center" style="width: 20%">Mallumoti</div>
                        <div class="text-center" style="width: 20%">Mutaxasisligi</div>
                       
                    </div>
                    @foreach($teachers as $te)  
                    <div  id="new{{$d++}}" style="display: none" class="d-flex mb-4 mt-1 ">
                        <div class="text-center " style="width: 20%">
                            <div style=" margin:0 auto;width:50px;height:50px; border-radius:50%;background-size:cover; background-image: url({{asset('/storage/teachers/'.$te->img)}})">
                               
                            </div>
                            <div class="portfolio-info">
                                <a href="{{asset('/storage/teachers/'.$te->img)}}" data-gall="porfolioGallery" class="venobox preview-link" ><i class="bx bx-plus"></i></a>
                            </div>
                        </div>
                       
                        <div class="text-center" style="width: 20%">
                        <p>{{$te->name}}</p>
                        </div>
                    
                        <div class="text-center" style="width: 20%">
                            <p>{{$te->age}}</p>
                        </div>
                        <div class="text-center" style="width: 20%">
                            <p>{{$te->malumot}}</p>
                        </div>
                        
                        <div class="text-center" style="width: 20%">
                            <p>{{$te->fani}}</p>
                        </div>
                        
                    </div>         
                    @endforeach
                    <button id="yana" class="btn btn-primary mt-5">Ko'proq ko'rish</button>         
                                    
                                
            </div>
            
        </div>
    </div>
  </section>
@endsection