@extends('layouts.user')

@section('content')

<section id="" class="portfolio">
    <div class="" data-aos="fade-up">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 ">
                <div class="section-title">
                    <h2>Maktab Gallerya</h2>
                </div>

                <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
                    <li data-filter="*" class="filter-active">Hammasi</li>
                    @foreach($tur as $tu)   
                    <li data-filter=".{{$tu->id}}">{{$tu->name}}</li>
                    @endforeach
                </ul>

                <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
                    @foreach($rasm as $ra)
                        <div class="col-lg-4 col-md-6 col-12 portfolio-item {{$ra->tur}}">
                            <div class="portfolio-img"><img src="{{ asset('storage/rasm')}}/{{$ra->img}}" class="w-100" alt=""></div>

                            <div class="portfolio-info">
                                <a href="{{ asset('storage/rasm')}}/{{$ra->img}}" data-gall="portfolioGallerys" class="venobox preview-link"><i class="bx bx-plus"></i></a>
                            </div>
                        </div>
                    @endforeach
                
                </div>
            </div>
           
        </div>
    </div>
  </section>
@endsection