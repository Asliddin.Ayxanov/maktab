@extends('layouts.user')

@section('content')

<section id="" class="portfolio">
    <div class="" data-aos="fade-up">
        <div class="row">
            <div class="col-12 col-md-9 ">
                <h1 class="text-center">Maktab Tarixi</h2>
                <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut quisquam ipsum maxime dolorum aut. Delectus libero sint quidem sapiente laudantium quam, dolorum maxime ipsum, consectetur nulla harum iusto exercitationem quasi.
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Temporibus accusamus saepe architecto iure laboriosam quos veritatis sapiente enim minima nesciunt quae qui repudiandae ratione nemo, ullam dolores accusantium eum libero!
                </p>
                 <div class="col">
                    <img src="{{ asset('rasmlar/maktab2.jpg')}}" class="img-fluid animated mb-2" alt="">
                </div>
                <div class="col">
                    <img src="{{ asset('rasmlar/maktab1.jpg')}}" class="img-fluid animated mb-2" alt="">
                </div>
                <div class="col">
                    <img src="{{ asset('rasmlar/maktab.jpg')}}" class="img-fluid animated mb-2" alt="">
                </div>
            </div>
            <div class="col-12 col-md-3 " style="
            background-color: #f2f2f2;">
                
                <div class="section-title">
                    <h2 id="as1">Yangiliklar</h2>
                </div>
                @foreach($news as $new)
                    <ul id="new{{$d++}}" class="list-group list-group-flush" style="display: none">
                        <li class="list-group-item" style="border-bottom: 5px solid rgb(12, 132, 211); "><a class="text-body" href="/yangilik/{{$new->id}}" ><b><i class="icofont-ui-calendar"></i>{{$new->created_at}}<br>  {{$new->titil}}</b></a></li>
                    </ul>
                @endforeach 
                <button id="yana" class="btn btn-primary mt-5">Ko'proq ko'rish</button>
            </div>
        </div>
    </div>
  </section>
@endsection