@extends('layouts.user')

@section('content')

<section id="" class="portfolio-details">
    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-12 col-md-8 ">
                <div class="portfolio-details-container">

                    <div class="owl-carousel portfolio-details-carousel ">
                      <img src="{{ asset('storage/raxbariyat')}}/{{$raxbar->img}}" class="" alt="" style="width:50%;">
                      
                    </div>
          
                    <div class="portfolio-info">
                      <h3>{{$raxbar->name}}</h3>
                      <ul>
                        <li><strong>Email</strong>:{{$raxbar->email}}</li>
                        <li><strong>Telefon</strong>:{{$raxbar->tel}}</li>
                        <li><strong>Lavozimi</strong>: {{$raxbar->lavozim}}</li>
                      </ul>
                    </div>
          
                  </div>
          
                  <div class="portfolio-description">
                    <h2>Maktab {{$raxbar->lavozim}} vazifalari</h2>
                    <p>
                       {{$raxbar->vazifasi}}
                    </p>
                </div>
               
                
             
            </div>
            <div class="col-12 col-md-4 "   style="
            background-color: #f2f2f2;">
                <div class="section-title">
                    <h2>Raxbarlaar</h2>
                </div>
                @foreach($raxbars as $new)
                    <ul id="new" class="list-group list-group-flush" >
                        <li class="list-group-item" style="border-bottom: 5px solid rgb(12, 132, 211);"><a class="text-body" href="/manager/{{$new->id}}">  {{$new->name}}</a></li>
                
                    </ul>
                @endforeach 
              
            </div>
        </div>
    </div>
  </section>
@endsection