<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Maktab</title>
   
 
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/venobox/venobox.css')}}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/aos/aos.css')}}" rel="stylesheet">
  <link href="{{ asset('assets/css/all.min.css')}}" rel="stylesheet" >
  <!-- Template Main CSS File -->
  <link href="{{ asset('assets/css/styleadmin.css')}}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: iPortfolio - v2.0.1
  * Template URL: https://bootstrapmade.com/iportfolio-bootstrap-portfolio-websites-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
    @if(!empty(Auth::user()->id))
  <!-- ======= Mobile nav toggle button ======= -->
  <button type="button" class="mobile-nav-toggle d-xl-none"><i class="icofont-navigation-menu"></i></button>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="d-flex flex-column">

      <nav class="nav-menu">
        <ol>
          <li class="@if($active ==1) active @endif
            "><a href="/home"><i class="bx bx-news"></i> <span>Yangiliklar</span></a></li>
          <li class="@if($active ==2) active @endif"><a href="/photo"><i class="bx bx-photo-album"></i> <span>Rasmlar</span></a></li>
          <li class="@if($active ==6) active @endif"><a href="/raxbariyat"><i class="bx bx-group"></i> <span>Raxbariyat</span></a></li>
          <li class="@if($active ==3) active @endif"><a href="/teacher"><i class="bx bx-group"></i> <span>O'qituvchilar</span></a></li>
          <li class="@if($active ==4) active @endif"><a href="/student"><i class="bx bx-group"></i> O'quvchilar</a></li>
          <li class="@if($active ==7) active @endif"><a href="/library"><i class="icofont-library"></i>Kutubxona</a></li>
          <li class="">
            <a  href="{{ route('logout') }}" onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                   <i class="bx bxs-exit"></i>Chiqish
            </a>
               
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
          </li>

        </ol>
      </nav><!-- .nav-menu -->
      <button type="button" class="mobile-nav-toggle d-xl-none"><i class="icofont-navigation-menu"></i></button>

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
 
    
 
@endif
  <main id="main">

    @yield('content')
  </main><!-- End #main -->
  {{-- <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a> --}}

  <!-- Vendor JS Files -->
  <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{ asset('assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/counterup/counterup.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/venobox/venobox.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/typed.js/typed.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/aos/aos.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets/js/mainadminn.js')}}"></script>
  <script src="{{ asset('assets/js/all.min.js')}}"></script>

</body>

</html>