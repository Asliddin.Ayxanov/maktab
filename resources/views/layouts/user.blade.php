<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>13-maktab</title>
  <meta content="" name="description">    
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta content="" name="keywords">
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
<!-- Vendor CSS Files -->
  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="{{ asset('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i') }}" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/aos/aos.css" rel="stylesheet') }}">

  <!-- Template Main CSS File -->
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Arsha - v3.0.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="/">13-MAKTAB</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li ><a href="/">Yangiliklar</a></li>
          <li  class="drop-down" ><a href="#portfolio">Maktab haqida</a>
            <ul>
              <li><a href="/maktab">Maktab Tarixi</a></li>
              <li><a href="/manager">Raxbarlar</a></li>
              <li><a href="/teachers">O'qituvchilar</a></li>
            </ul>
          </li>
          <li  class="drop-down">
            <a href="">O'quvchilarga</a>
            <ul>
              <li><a href="/jadval">Dars jadvali</a></li>
              <li><a href="/kutubxona">Kutubxona</a></li>
              <li><a href="/talabalar">Iqtidorli o'quvchilar</a></li>
            </ul>
          </li>
          <li><a href="/gallerya">Gallerya</a></li>
          <li><a href="#contact">Biz bilan bog'lanish</a></li>
        </ul>
      </nav>
      <a href="https://kundalik.com" class="get-started-btn scrollto">Kundalik.com</a>
    </div>
  </header>
  <section id="hero" class="d-flex align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <h1>Maktabning to'liq nomi</h1>
          <h2>Maktab shiyori yoki yil nomi yozamizmi</h2>
          <div class="d-lg-flex">
            <a href="#about" class="btn-get-started scrollto">Yangiliklar</a>
            
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="{{ asset('rasmlar/school3.jpg')}}" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">
    
    @yield('content')
  </main>
<section id="contact" class="contact bg-dark">
    <div class="container" >
  
      <div class="section-title">
        <h2 class="text-white">Biz bilan bog'lanish</h2>
        <p class="text-white">Sizning fikringiz biz uchun muhum</p>
      </div>
  
      <div class="row bg-dark">
  
        <div class="col-12   col-lg-6  bg-dark">
          <div class="info bg-dark">
            <div class="address">
              <i class="icofont-google-map"></i>
              <h4 class="text-white">Manzil:</h4>
              <p class="text-white">Jizzax viloyati G'allaorol tumani 54-Maktab</p>
            </div>
  
            <div class="email">
              <i class="icofont-envelope"></i>
              <h4 class="text-white">Email:</h4>
              <p class="text-white">info@example.com</p>
            </div>
  
            <div class="phone">
              <i class="icofont-phone"></i>
              <h4 class="text-white">Telefon:</h4>
              <p class="text-white">+998995848650</p>
            </div>
  
          </div>
  
        </div>
        <div class="col-12  col-lg-6  bg-dark">
          <div class="info bg-dark">
            <div class="address mb-3">
              <a href="https://www.uzedu.uz/" class="row">
                <div class="col-2">
                  <img src="{{asset('/rasmlar/xalqtlaim.jpg')}}" alt="" style="width: 100%">
                </div>
                <div class="col-10">
                  <h4 class="text-white">O'zbekiston Respublikasi xalq ta'lim vazirligi</h4>
                </div>
              </a>
            </div>
  
            <div class="email mb-3">
              <a href="https://www.uzedu.uz/" class="row">
                <div class="col-2">
                  <img src="{{asset('/rasmlar/kundalik.png')}}" alt="" style="width: 100%">
                </div>
                <div class="col-10">
                  <h4 class="text-white">Kundalik.com</h4>
                </div>
              </a>
            </div>
            <div class="email mb-3">
              <a href="https://www.uzedu.uz/" class="row">
                <div class="col-2">
                  <img src="{{asset('/rasmlar/kitob.jpeg')}}" alt="" style="width: 100%">
                </div>
                <div class="col-10">
                  <h4 class="text-white">O'zbekiston Respublikasi bolalar kutubxonasi</h4>
                </div>
              </a>
            </div>
            <div class="phone mb-3">
              <a href="https://www.uzedu.uz/" class="row">
                <div class="col-2">
                  <img src="{{asset('/rasmlar/ziyo.jpg')}}" alt="" style="width: 100%">
                </div>
                <div class="col-10">
                  <h4 class="text-white">Ziyo.net ta'lim portali</h4>
                </div>
              </a>
            </div>
  
          </div>
  
        </div>
  
      </div>
  
    </div>
  </section><!-- End Contact Section -->
  
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->

  <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{ asset('assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/venobox/venobox.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/aos/aos.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets/js/main.js')}}"></script>
  <script src="{{ asset('/js/jquery.js')}}"></script>
</section>
<script>
  $(document).ready(function(){
  var d=8;
  for(var i=1;i<=d;i++){
    $('#new'+i).show();
  
  }
  $('#yana').click(function(){
    var t=d+1;
  d=d+8;
  for(var i=t;i<=d;i++)
    {
        $('#yana').remove();
        $('#new'+i).show();
        $('#naw'+i).before('<button id="yana" class="btn btn-primary mt-5">Ko\'proq ko\'rish</button>');
    }
});
});
</script>

</body>

</html>