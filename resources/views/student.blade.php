@extends('layouts.user')

@section('content')

<section id="" class="team section-bg">
    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-12 col-md-8 ">
                <div class="section-title">
                    <h2>Iqtidorli O'quvchilar</h2>
                </div>
                <div class="row">
                    @foreach ($student as $raxbar)
                    
                    <div class="col-12">
                        <div class="member d-flex  align-items-center" data-aos="zoom-in" data-aos-delay="100">
                            <div style="overflow: hidden;width: 100%;">
                                <img src="{{ asset('storage/student')}}/{{$raxbar->img}}" class="img-fluid" alt="">
                            </div>
                            <div class="member-info text-center " >
                                <h4>{{$raxbar->name}}</h4>
                                <span>{{$raxbar->malumot}}</span>
                               <hr style="border: 3px splid blue">
                              
                               
                                
                            </div>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
               
                
             
            </div>
            <div class="col-12 col-md-4 "  style="
            background-color: #f2f2f2;">
                <div class="section-title">
                    <h2>Kitob javoni</h2>
                </div>
                @foreach($sinf as $new)
                    <ul id="new" class="list-group list-group-flush" >
                        <li class="list-group-item" style="border-bottom: 5px solid rgb(12, 132, 211);"><a class="text-body" href="/yangilik/{{$new->id}}">  {{$new->name}}</a></li>
                
                    </ul>
                @endforeach 
              
            </div>
        </div>
    </div>
  </section>
@endsection