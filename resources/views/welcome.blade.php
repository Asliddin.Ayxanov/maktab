@extends('layouts.user')

@section('content')
<section id="about" class="about">
  <div class="container" data-aos="fade-up">

    <div class="section-title">
      <h2 id="as1">Yangiliklar</h2>
    </div>
    <div class="row">
        @foreach($news as $new)
        <div id="new{{$d++}}" class="card col-12 col-md-6 col-lg-3 mb-5" style="display: none">
          <div class="portfolio-img"><img src="{{asset('/storage/news/'.$new->img)}}" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <a href="{{asset('/storage/news/'.$new->img)}}" data-gall="porfolioGallery" class="venobox preview-link" ><i class="bx bx-plus"></i></a>
            <p><i class="icofont-ui-calendar"></i>{{$new->created_at}}</p>
          </div>
         
          <div class="card-body">
          <h5 class="card-title">{{$new->titil}}</h5>
          <a href="/yangilik/{{$new->id}}" class="btn btn-primary">Batafsil</a>
          </div>
        </div>
        @endforeach 
        <button  id="yana" class="btn btn-primary">Ko'proq ko'rish</button>
    </div>
   

  </div>

@endsection

