@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 ">
            <a href="/home" class="btn btn-outline-primary m-4">  
                 <span id="qayt" class="fas fa-chevron-left "></span>
            </a>
        </div>
        <div class="col-12 ">
            <h1 class="text-center">Yangilik qo'shish</h1>
        </div>
        <div class="col-9 mb-5">
            <form action="/news/qoshish/save" method="post" enctype="multipart/form-data">
                <div class="form-group mb-5 mt-5">
                    <label  for="titil" >Sarlovha</label>
                    <textarea type="text" rows="10" class="form-control" id="Titil" name="titil" placeholder="Sarlovha"></textarea>
                </div>
                @error('titil')
                    <div class="alert alert-danger">10 ta belgidan ko'p matn kiriting</div>
                @enderror
                <div class="form-group mb-5">
                    <label for="mss">Matn</label>
                    <textarea type="text" rows="10" class="form-control" id="mss"  name="mss" placeholder="Matn"></textarea>
                </div>
                @error('mss')
                    <div class="alert alert-danger">20 ta belgidan ko'p matn kiriting</div>
                @enderror
                <div class="form-group mb-5 mt-5">
                    <label  for="birthdaytime" >Sanasi</label>
                    <input type="datetime-local" id="birthdaytime" name="sana">
                </div>
                @error('sana')
                    <div class="alert alert-danger">Namunaga qarab kiriting</div>
                @enderror
                <div class="form-group mb-5">
                    <label for="file">Rasm</label>
                    <label for="file" class="bg-info text-white h3" style="display:flex; justify-content:center;width:100%;border-radius:3em; height:40px;align-items:center;">
                        <svg class="bi bi-download" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M.5 8a.5.5 0 0 1 .5.5V12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V8.5a.5.5 0 0 1 1 0V12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V8.5A.5.5 0 0 1 .5 8z"/>
                            <path fill-rule="evenodd" d="M5 7.5a.5.5 0 0 1 .707 0L8 9.793 10.293 7.5a.5.5 0 1 1 .707.707l-2.646 2.647a.5.5 0 0 1-.708 0L5 8.207A.5.5 0 0 1 5 7.5z"/>
                            <path fill-rule="evenodd" d="M8 1a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0v-8A.5.5 0 0 1 8 1z"/>
                        </svg>
                    </label>
                    <input type="file" id="file"  name="imgs" style="display:none;">
                </div>
                @error('imgs')
                    <div class="alert alert-danger">Rasm yuklang (jpeg,bmp,png,jpg)</div>
                @enderror
                <input type="hidden"  name="page" value="{{$page}}">
                <button type="submit" class="btn btn-primary ">Saqla</button>
                {{csrf_field()}}
            </form>
        </div>
    </div>
</div>
@endsection