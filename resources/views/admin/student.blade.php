@extends('layouts.app')

@section('content')

<nav class="navbar bg-light navbar-light navbar-expand-lg w-100">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-7">
                <a class="navbar-brand btn btn-outline-success" href="/student/qoshish" class="img-fluid" style="color: seagreen;">O'quvchi qo'shish</a>
            </div>
        
            <form class="form-inline  col-12 col-md-5 col-lg-4">
                <input class="form-control mr-sm-2 w-10" type="text" placeholder="Search" name="sms" value="{{$sms}}">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            
            <div class="col-12 col-md-1">
                <a class="btn btn-danger " href="/student?clear=1">Clear</a>
            </div>
        </div>
    </div>
</nav>


<div class="d-flex mb-4 mt-1">
    <div class="text-center" style="width: 5%">Id</div>
    <div class="text-center" style="width: 25%">FIO</div>
    <div class="text-center" style="width: 30%">Mallumoti</div>
    <div class="text-center" style="width: 25%">Rasm</div>
    <div class="text-center" style="width: 20%">Amallar</div>
</div>
@foreach($students as $te)  
<div class="d-flex mb-4 mt-1">
    <div class="text-center" style="width: 5%">{{++$d}}</div>

    <div class="text-center" style="width: 25%">
        <p>{{$te->name}}</p>
    </div>
   
   
    <div class="text-center" style="width: 30%">
        <p>{{$te->malumot}}</p>
    </div>
    
    <div class="text-center" style="width: 25%">
        <div class="portfolio-img"><img src="{{asset('/storage/student/'.$te->img)}}" style="max-height: 15vh"  alt=""></div>
        <div class="portfolio-info">
            <a href="{{asset('/storage/student/'.$te->img)}}" data-gall="porfolioGallery" class="venobox preview-link" ><i class="bx bx-plus"></i></a>
        </div>
    </div>

    <div class="text-center" style="width: 20%">
       
        <a class="btn btn-primary" href="/student/edit/{{$te->id}}">Edit</a>
        <a class="btn btn-danger" href="/student/delete/{{$te->id}}">Delete</a>
    </div>
</div>         
@endforeach
@endsection
