@extends('layouts.app')

@section('content')

<nav class="navbar bg-light navbar-light navbar-expand-lg w-100">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-7">
                <a class="navbar-brand btn btn-outline-success" href="/news/qoshish?page={{$page}}" style="color: seagreen;">Yangilik qo'shish</a>
            </div>
        
            <form class="form-inline  col-12 col-md-5 col-lg-4">
                <input class="form-control mr-sm-2 w-10" type="text" placeholder="Search" name="sms" value="{{$sms}}">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            
            <div class="col-12 col-md-1">
                <a class="btn btn-danger " href="/home?clear=1">Clear</a>
            </div>
        </div>
    </div>
</nav>
{{$news->links()}}

<div class="d-flex mb-4 mt-1">
    <div class="text-center" style="width: 5%">Id</div>
    <div class="text-center" style="width: 35%">Sarlavha</div>
    <div class="text-center" style="width: 20%">Sana</div>
    <div class="text-center" style="width: 20%">Rasm</div>
    <div class="text-center" style="width: 20%">Amallar</div>
</div>
@foreach($news as $new)  
<div class="d-flex mb-4 mt-1">
    <div class="text-center" style="width: 5%">{{++$d}}</div>
    <div class="text-center" style="width: 35%">
    <p>{{$new->titil}}</p>
    </div>
    <div class="text-center" style="width: 20%">
        <p>{{$new->created_at}}</p>
        </div>
   
    <div class="text-center" style="width: 20%">
        <div class="portfolio-img"><img src="{{asset('/storage/news/'.$new->img)}}" class="img-fluid" alt=""></div>
        <div class="portfolio-info">
          <a href="{{asset('/storage/news/'.$new->img)}}" data-gall="porfolioGallery" class="venobox preview-link" ><i class="bx bx-plus"></i></a>
        </div>
    </div>
    <div class="text-center" style="width: 20%">
        <button class="btn btn-primary" data-toggle="modal" data-target="#ex{{$new->id}}">Read</button>
        <div class="modal fade" id="ex{{$new->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <p class="modal-title" id="staticBackdropLabel">{{$new->titil}}</p>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{$new->mss}}</p>
                      </div>
                </div>
            </div>
        
        </div>
        <a class="btn btn-primary" href="/news/edit/{{$new->id}}?page={{$page}}">Edit</a>
        <a class="btn btn-danger" href="/news/delete/{{$new->id}}?page={{$page}}">Delete</a>
    </div>
</div>         
@endforeach
@endsection
