@extends('layouts.app')

@section('content')

<nav class="navbar bg-light navbar-light navbar-expand-lg w-100">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-7">
                <a class="navbar-brand btn btn-outline-success" href="/library/qoshish" class="img-fluid" style="color: seagreen;">Kitob qo'shish</a>
            </div>
            <form class="form-inline  col-12 col-md-5 col-lg-4">
                <select id="inputState" class="form-control" name="sms" >
                    
                    <option value="0">hammasi</option>
                    @foreach ($sinf as $tu)
                    <option value="{{$tu->id}}"> {{$tu->name}}</option>
                    @endforeach
                    
                   </select>
                   <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            <div class="col-12 col-md-1">
                <a class="btn btn-danger " href="/library?clear=1">Clear</a>
            </div>
        </div>
    </div>
</nav>


<div class="d-flex mb-4 mt-1">
    <div class="text-center" style="width: 10%">Id</div>
    <div class="text-center" style="width: 30%">FIO</div>
    <div class="text-center" style="width: 30%">Turi</div>
    
    <div class="text-center" style="width: 30%">Amallar</div>
</div>
@foreach($books as $te)  
<div class="d-flex mb-4 mt-1">
    <div class="text-center" style="width: 10%">{{$d++}}</div>
    <div class="text-center" style="width: 30%">
        <p>{{$te->name}}</p>
    </div>
    <div class="text-center" style="width: 30%">
        <p>
            @foreach($sinf as $sin)  
                @if($sin->id==$te->sinf)
                    {{$sin->name}}
                @endif
            @endforeach
        </p>
    </div>
    <div class="text-center" style="width: 30%">
       
        <a class="btn btn-primary" href="/library/edit/{{$te->id}}">Edit</a>
        <a class="btn btn-danger" href="/library/delete/{{$te->id}}">Delete</a>
    </div>
</div>         
@endforeach
@endsection
