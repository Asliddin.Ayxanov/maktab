@extends('layouts.app')

@section('content')
<nav class="navbar bg-light navbar-light navbar-expand-lg w-100">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-7">
                <a class="navbar-brand btn btn-outline-success" href="/rasm/qoshish" style="color: seagreen;">Rasm qo'shish</a>
            </div>
        </div>
    </div>
</nav>
<section id="portfolio" class="portfolio">
  <div class="container" data-aos="fade-up">

    <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
            <li data-filter="*" class="filter-active">Hammasi</li>
        @foreach($tur as $tu)   
            <li data-filter=".{{$tu->id}}">{{$tu->name}}</li>
        @endforeach
    </ul>
    {{$rasm->links()}}

    <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

        @foreach($rasm as $ra)
            <div class="col-lg-4 col-md-6 portfolio-item {{$ra->tur}}">
            <div class="portfolio-img"><img src="{{ asset('storage/rasm')}}/{{$ra->img}}" class="img-fluid" alt=""></div>
                <div class="portfolio-info">
                
                   
                        <a href="{{asset('/storage/rasm/'.$ra->img)}}" data-gall="porfolioGallery" class="venobox preview-link" ><i class="bx bx-plus"></i></a>
                       
                  
                <a href="/rasm/delete/{{$ra->id}}" class="details-link" title="More Details"><i class="fas fa-trash-alt"></i></a>
                </div>
            </div>
        @endforeach
    </div>
</div>
</section>
@endsection
