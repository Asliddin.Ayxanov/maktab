<?php

use Illuminate\Database\Seeder;
use App\Sinf;
class SinfSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        $tur=[
            '1-sinf',
            '2-sinf',
            '3-sinf',
            '4-sinf',
            '5-sinf',
            '6-sinf',
            '7-sinf',
            '8-sinf',
            '9-sinf',
            '10-sinf',
            '11-sinf',
            'Badiy adabiyotlar',
            'Qo\'llanmalar',
        ];
        foreach($tur as $t){
            $tabla=new Sinf();
            $tabla->name=$t;
            $tabla->save();
        }
    }
}
