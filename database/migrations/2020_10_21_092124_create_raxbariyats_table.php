<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRaxbariyatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raxbariyats', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('img');
            $table->string('lavozim');
            $table->string('email');
            $table->string('tel');
            $table->text('vazifasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raxbariyats');
    }
}
